"""Exports EPS or SVG file (vector image), from `outlines` list of outlines.

Each outline is a lists of pixels = 2-tuples with 1 (black) / 0 values.

Main function is here
    `save_eps_from_outlines(outlines, filename, dim=None)`
and
    `save_svg_from_outlines(outlines, filename, dim=None, filled=False)`
for creating the corresponding file.

In addition,
    `eps_content_from_outlines(outlines, dim)`
    `svg_content_from_outlines(outlines, dim)`
return the content of such a file, as a string,
where dim represent image dimensions as (width, height) tuple. 

These dimensions (say, minimal ones) may be given by 
    `dim_from_outlines(outlines)`
"""
# About EPS and SVG formats comparison :
#  https://pediaa.com/what-is-the-difference-between-eps-and-svg/#EPS
#  https://www.educba.com/svg-vs-eps/
#  https://fr.vectormagic.com/support/file_formats [FR]

# Using get_current_dir(), cause os.getcwd() doesn't work within some IDEs

def dim_from_outlines(outlines):
    """Return (minimal) dimensions (= 2-tuple) of image, from `outlines` list.
    """
    width = max([max([pix[0] for pix in outl]) for outl in outlines])
    height = max([max([pix[1] for pix in outl]) for outl in outlines])
    return width+1 , height+1

# First only content, no file created

def eps_content_from_outlines(outlines, dim):
    """Convert `outlines` list into EPS file content.
    """
    # Specs:  https://www-cdf.fnal.gov/offline/PostScript/5002.PDF
    width, height = dim
    eps = "%!PS-Adobe-3.0 EPSF-3.0\n"
    eps += "%%BoudingBox: 0 0 " + str(width) + " " + str(height) + "\n"
    
    #unuseful with filling
    #eps += "newpath\n0.1 setlinewidth\nnewpath\n"
    for outline in outlines:
        # In EPS files, y-axis is bottom-up, opposite to outline list
        eps += str(outline[0][0]) + " " + str(height - outline[0][1])
        eps += " moveto\n" 
        for pixel in outline[1:]:
            eps += str(pixel[0]) + " " + str(height - pixel[1]) + " lineto\n"
    # Fill is automaticly correct, depending on rotation orientation (in / out)
    eps += "closepath\nfill\n"
    return eps
    
def svg_content_from_outlines(outlines, dim, filled=False):
    """Convert `outlines` list into SVG file content.
    
    When `filled` is False: no stroke, a 0.3 fill-opacity to distinguish layers.
    """
    width, height = dim
    svg = '<?xml version="1.0" standalone="no"?>\n'
    svg += '<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" '
    svg += '"http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">\n'
    svg += '<svg width="' + str(width) + '" height="' + str(height) + '" '
    if filled:
        svg += 'fill="#FF0000" stroke="none" fill-opacity="0.3">\n'
    else:
        svg += 'fill="none" stroke="#000000">\n'
    for outline in outlines:
        svg += '  <path d="M' + str(outline[0][0]) + ',' + str(outline[0][1])
        # Pour limiter la longueur des lignes
        cpt = 0
        for pixel in outline[1:]:
            svg += ' L' + str(pixel[0]) + ',' + str(pixel[1])
            cpt = (cpt + 1) % 10
            if cpt == 9: svg += '\n'
        svg += '" />\n'
    svg += '</svg>'
    return svg

# File created

def save_eps_from_outlines(outlines, filename, dim=None):
    """Create EPS file named by `filename` and created from `outlines`.
    
    Dimensions are optionally given by `dim`.
    If `multicolored` is True, 8 different colors are used for outlines.
    """
    if not dim:
        dim = dim_from_outlines(outlines)
    eps = eps_content_from_outlines(outlines, dim)
    try:
        with open(filename, "w", encoding="ascii") as f:
            f.write(eps)
    except IOError:
        print("Unable to write", filename, "sorry !")
    
def save_svg_from_outlines(outlines, filename, dim=None, filled=False):
    """Create SVG file named by `filename` and created from `outlines`.
    
    Dimensions are optionally given by `dim`.
    When `filled` is False: no stroke, a 0.3 fill-opacity to distinguish layers.
    """
    if not dim:
        dim = dim_from_outlines(outlines)
    svg = svg_content_from_outlines(outlines, dim, filled)
    try:
        with open(filename, "w", encoding="ascii") as f:
            f.write(svg)
    except IOError:
        print("Unable to write", filename, "sorry !")

if __name__ == "__main__":
    
    # Using get_current_dir(), because os.getcwd() doesn't work within some IDEs
    import inspect, os.path 
    #
    def get_current_dir():
        """Return current directory, ended by '/' (works even launched by IDE).
        """
        # Thanks to 
        # https://stackoverflow.com/questions/2632199/
        #           how-do-i-get-the-path-of-the-current-executed-file-in-python
        #
        # Needs import inspect, os.path
        filename = inspect.getframeinfo(inspect.currentframe()).filename
        path = os.path.dirname(os.path.abspath(filename))
        return path + os.sep
    #
    os.chdir(get_current_dir())
    #
    from pix2outlines import outlines_from_pixels
    
    # Demo example
    ex = [[0, 0, 1, 1, 1, 1, 0],
          [0, 1, 1, 1, 0, 1, 0],
          [0, 1, 0, 0, 0, 1, 1],
          [0, 1, 1, 1, 1, 1, 0],
          [0, 0, 1, 1, 1, 0, 0],
          [1, 0, 0, 0, 0, 0, 0],
          [1, 1, 1, 0, 0, 0, 0],
          [1, 1, 1, 0, 1, 1, 1],
          [0, 1, 1, 1, 1, 1, 1]]
    FILLED = True # True is better for very small images
    
    outl = outlines_from_pixels(ex)
    
    # eps = eps_from_outlines(outl, dim_from_outlines(outl))
    # print(eps)
    filename = get_current_dir() + "output/test.eps"
    save_eps_from_outlines(outl, filename, dim_from_outlines(outl))
    
    # svg = svg_from_outlines(outl, dim_from_outlines(outl))
    # print("SVG\n", svg)
    filename = get_current_dir() + "output/test.svg"
    save_svg_from_outlines(outl, filename, dim_from_outlines(outl), FILLED)
    
    print("Done !\nJust open test.eps and test.svg in ./output/ directory...")