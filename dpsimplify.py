"""Douglas-Peucker simplifiying list of outlines, for black and white images.

Main function is here
    `douglas_peucker_outlines_simplify(outlines, threshold)`
where `threshold` determines if a point has to be excluded or not, 
from each outline of the list (point is removed if its distance from a segment
simplifying outline is greater than `threshold`).
It returns `outlines` list, as input.

In addition,
    `stats(outlines)`
returns the number of points composing outlines.
"""
from math import sqrt

def segment_dist(M, A, B):
    """Return distance between point M and segment [AB].
    
    Each variable is a 2-tuple representing coordinates of a point."""
    
    # assert type(M) is tuple and len(M) == 2
    # assert type(A) is tuple and len(A) == 2
    # assert type(B) is tuple and len(B) == 2
    
    # Three cases are :
    #                         +M3     
    #   M1+  :            :  /
    #      \ :   M2+      : /
    #       \:     |      :/
    #      A +-----+------+ B
    #        :     P      :
    #        :            :
    
    xM, yM = M 
    xA, yA = A 
    xB, yB = B 
    if xA == xB and yA == yB: # dist = AM
        return sqrt((xM-xA)**2 + (yM-yA)**2)
    xAM, yAM = xM-xA, yM-yA
    xAB, yAB = xB-xA, yB-yA
    k = (xAM*xAB + yAM*yAB) / (xAB**2 + yAB**2)
    if k <= 0: # dist = AM, like with M1 on figure
        return sqrt((xM-xA)**2 + (yM-yA)**2)
    if k >= 1: # dist = BM, like with M3
        return sqrt((xM-xB)**2 + (yM-yB)**2)
    # dist = PM,  where P = projection of M on [AB], like with M2
    xP, yP = xA + k*(xB-xA), yA + k*(yB-yA)
    return sqrt((xM-xP)**2 + (yM-yP)**2)

def rec_douglas_peucker(outline, i1, i2, threshold):
    """Return simplified outline, between i1 and i2 indexes, regarding threshold.
    
    A recursive function, mainly useful for global outline simplification with
        `douglas_peucker_simplify(outline, threshold)`
    call.
    """
    dist_max = 0
    index_max = i1
    A, B = outline[i1], outline[i2]
    for i in range(i1+1, i2):
        dist_i = segment_dist(outline[i], A, B)
        if dist_max < dist_i:
            dist_max = dist_i
            index_max = i
    if dist_max <= threshold:
        # Simplified ouline = one segment
        return [outline[i1], outline[i2]]
    else:
        # Recursive calls then join
        outl1 = rec_douglas_peucker(outline, i1, index_max, threshold)
        outl2 = rec_douglas_peucker(outline, index_max, i2, threshold)
        outl1.extend(outl2[1:]) # without outl2[0], the same as last point of outl1
        return outl1

def douglas_peucker_simplify(outline, threshold):
    """Return simplified (unique) outline, regarding threshold.
    
    Threshold determines if a point has to be excluded from outline 
    (when distance from simplified outline is greater than threshold) or not.
    """
    return rec_douglas_peucker(outline, 0, len(outline)-1, threshold)

def douglas_peucker_outlines_simplify(outlines, threshold):
    """Return list of simplified outlines, regarding threshold.
    
    Threshold determines if a point has to be excluded from path 
    (if its distance from simplified path is greater than threshold) or not.
    """
    return [douglas_peucker_simplify(outl, threshold) for outl in outlines]

def stats(outlines):
    """Returns the number of points (2-tuples) contained in `outlines`
    
    Argument may be a list of tuples (single outline) or 
    a list of lists of tuples (list of outlines)
    """
    if type(outlines[0]) is tuple: 
        return len(outlines)
    return sum(len(outline) for outline in outlines)

if __name__ == "__main__":
    # Using get_current_dir(), cause os.getcwd() doesn't work within some IDEs
    import inspect, os.path 
    def get_current_dir():
        """Return current directory, ended by '/' (works even when launched by IDE).
        """
        # Thanks to 
        # https://stackoverflow.com/questions/2632199/
        #              how-do-i-get-the-path-of-the-current-executed-file-in-python
        #
        # Needs import inspect, os.path
        filename = inspect.getframeinfo(inspect.currentframe()).filename
        path = os.path.dirname(os.path.abspath(filename))
        return path + os.sep
    os.chdir(get_current_dir())
    from pix2outlines import outlines_from_pixels
    
    # Same as ex2.png
    p = [(1,0), (2,0), (2,1), (3,1), (4,1), (4,2), (4,3), (5,3), (6,3),
        (6,4), (5,4), (4,4), (3,4), (3,3), (3,2), (2,2), (2,3), (2,4),
        (1,4), (1,3), (0,3), (0,2), (0,1), (1,1), (1,0)]
    THRESHOLD = 0.9 # (*)

    simplified_outlines = douglas_peucker_simplify(p, THRESHOLD)     
    n1 = stats(p)
    print("Before:", n1, "points")
    n2 = stats(simplified_outlines)
    print("After, with ", THRESHOLD, " threshold: ", n2 , " points (",
           "{:.1f}%".format(100 * (n2-n1) / n1), ")", sep="")
           
    #print("One outline : from", p, "to",simplified_outlines, sep="\n")
    
    # (*) Note that it is incorrect with 1.0 threshold: 
    #  before last point (0, 1) is missing, because of improper float numbers 
    #  binary representation.
    
    # Same as ex1.png and example in pix2outline.py
    ex1 = [[0, 0, 1, 1, 1, 1, 0],
           [0, 1, 1, 1, 0, 1, 0],
           [0, 1, 0, 0, 0, 1, 1],
           [0, 1, 1, 1, 1, 1, 0],
           [0, 0, 1, 1, 1, 0, 0],
           [1, 0, 0, 0, 0, 0, 0],
           [1, 1, 1, 0, 0, 0, 0],
           [1, 1, 1, 0, 1, 1, 1],
           [0, 1, 1, 1, 1, 1, 1]]
    THRESHOLD = 0.9
    
    outl = outlines_from_pixels(ex1)
    simplified_outlines = douglas_peucker_outlines_simplify(outl, THRESHOLD)
    
    n1 = stats(outl)
    print("\nBefore:", n1, "points")
    n2 = stats(simplified_outlines)
    print("After, with ", THRESHOLD, " threshold: ", n2 , " points (",
           "{:.1f}%".format(100 * (n2-n1) / n1), ")", sep="")

    #print("\nSeveral outlines : from", outl, "to", 
    #       simplified_outlines, sep="\n")