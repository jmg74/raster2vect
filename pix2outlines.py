"""Dectect Outlines from a black and white bitmap image

Works only on a pure black and white "image", say `pix`,
 . coded as a (Python) list of lists: `pix[x][y]` represents (x,y) pixel,
 . where 1 stands for black pixel and 0 for white one.
 
Main function is here
    `outlines_from_pixels(pix)`
that returns a list of outlines detected from `pix` image, 
where each outline is a list of 2-tuple (coordinates of each point).
"""

# Be aware of difference between pixels (squares) and points (zero thickness):
#         Eg. of pixels coordinates where (x,y) are ones of @ point
# 
#                          |           |
#                (x-1,y-1) |  (x,y-1)  | (x+1,y-1)
#                          |           |
#               -----------@-----------o-----------
#                          |           |
#                 (x-1,y)  |   (x,y)   |  (x+1,y)
#                          |           |
#               -----------o-----------o-----------
#                          |           |
#                (x-1,y+1) |  (x,y+1)  | (x+1,y+1)
#                          |           |

# No OOP here, to keep easy to adapt to an activity for my students...

from math import sqrt

EAST, NORTH, WEST, SOUTH = 0, 1, 2, 3

def is_white(x, y, pix):
    """True, iif the (x, y) pixel of `pix` is white or does not exit."""
    width, height = len(pix[0]), len(pix)
    # Out of frame
    if x < 0 or x >= width or y < 0 or y >= height:
        return True
    # Existing pixel
    return pix[y][x] == 0

def is_first_point(x, y, pix):
    """True if the (x, y) point of `pix` could be appropriate to begin an outline.
    
    That means 
     - this pixel is black and 
     - the north-east pixel is white.
    """
    return is_white(x,y-1,pix) and not is_white(x,y,pix)

def next_point(x, y, direction=EAST):
    """Return next (integer coordinates) point in the given `direction`
    """
    if direction == EAST:
        return x+1, y
    elif direction == NORTH:
        return x, y-1
    elif direction == WEST:
        return x-1, y
    else:
        return x, y+1

def left_direction(direction):
    """Return `direction` after a "left turn" (anticlockwise 90° rotation).
    """
    return (direction+1) % 4
    
def right_direction(direction):
    """Return `direction` after a "right turn" (clockwise 90° rotation).
    """
    return (direction -1) % 4
    
def left_pixel(x, y, direction):
    """Return coordinates of next pixel on the left, considering `direction'.
    """
    if direction == EAST:
        return x, y-1
    elif direction == NORTH:
        return x-1, y-1
    elif direction == WEST:
        return x-1, y
    else:
        return x, y
    
def right_pixel(x, y, direction):
    """Return coordinates of next pixel on the right, considering `direction'.
    """
    if direction == EAST:
        return x, y
    elif direction == NORTH:
        return x, y-1
    elif direction == WEST:
        return x-1, y-1
    else:
        return x-1, y

def outline_from_pixels(x, y, pix, taken):
    """Find an outline following the grid, from and to (x, y) point of `pix`.
    
    `taken[x][y]` is True when (x, y) point is already used to begin an outline,
    this variable is modified (side effect). 
    """
    line = []
    x0, y0 = x, y
    direction = EAST
    taken[x][y] = True
    line.append((x, y))
    # delta could be used to dectect inner / outer outlines (not used for now)
    #delta = 0
    while 1:
        x, y = next_point(x, y, direction)
        line.append((x, y))
        taken[x][y] = True
        if not is_white(*left_pixel(x, y, direction), pix):
            direction = left_direction(direction)
            #delta += 1
        elif is_white(*right_pixel(x, y, direction), pix):
            direction = right_direction(direction)
            #delta -= 1
        if (x, y) == (x0, y0) and direction == EAST:
            break
    # Inner outline
    #if delta == 4:
    #    pass
    return line
    
def outlines_from_pixels(pix):
    """Return a list of outlines (= lists of pixels as 2-tuples) found in `pix`.
    """
    outline_list = []
    width, height = len(pix[0]), len(pix)
    # Memorization of already used points to begin an outline 
    #   => O(1) time verification
    taken = [[0 for _ in range(height+1)] for _ in range(width+1)]
    for y in range(height):
        for x in range(width):
            if not taken[x][y] and is_first_point(x, y, pix):
                outline_list.append(outline_from_pixels(x, y, pix, taken))
    return outline_list
    
                
if __name__ == "__main__":
    
    # Same as ex1.png 
    ex1 = [[0, 0, 1, 1, 1, 1, 0],
           [0, 1, 1, 1, 0, 1, 0],
           [0, 1, 0, 0, 0, 1, 1],
           [0, 1, 1, 1, 1, 1, 0],
           [0, 0, 1, 1, 1, 0, 0],
           [1, 0, 0, 0, 0, 0, 0],
           [1, 1, 1, 0, 0, 0, 0],
           [1, 1, 1, 0, 1, 1, 1],
           [0, 1, 1, 1, 1, 1, 1]]
    
    outl = outlines_from_pixels(ex1)
    
    # Modifying order for x, y in above double loop may change the result
    assert outl == [[(2, 0), (3, 0), (4, 0), (5, 0), (6, 0), (6, 1), (6, 2), (7, 2), (7, 3), (6, 3), (6, 4), (5, 4), (5, 5), (4, 5), (3, 5), (2, 5), (2, 4), (1, 4), (1, 3), (1, 2), (1, 1), (2, 1), (2, 0)], [(2, 3), (3, 3), (4, 3), (5, 3), (5, 2), (5, 1), (4, 1), (4, 2), (3, 2), (2, 2), (2, 3)], [(0, 5), (1, 5), (1, 6), (2, 6), (3, 6), (3, 7), (3, 8), (4, 8), (4, 7), (5, 7), (6, 7), (7, 7), (7, 8), (7, 9), (6, 9), (5, 9), (4, 9), (3, 9), (2, 9), (1, 9), (1, 8), (0, 8), (0, 7), (0, 6), (0, 5)]]
    
    print("Checked !\n\noutlines =", outl)