"""From a bitmap image file (png, etc.) to a list of lists of B/W pixels.

Main function 
   imgfile2tab(filename)
returns a list of lists of pixels (2-tuples, of 1 for black or 0 values).

Use PIL (pillow) library.
"""
from PIL import Image

def bw_convert(img, threshold = 128):
   """Return a pure black and white image from a grayscale one.
   """
   return img.convert(mode="1").point(lambda x:(0 if x>=threshold else 1), 
                                      mode="1")

def imgfile2tab(filename):
   """Return a list of lists of 1 (black) or 0 values from an image file.
   """
   try:
      img = Image.open(filename)
   except IOError:
      print("Can't open", filename, "file, sorry.")
      return []
   bw = bw_convert(img)
   pixels = [[bw.getpixel((x,y)) for x in range(bw.width)] 
                                                     for y in range(bw.height)]
   return pixels

if __name__ == "__main__":

   import inspect, os.path

   FILE = "img/ex1.png" # "img/butterfly.png" # "img/france_map.png" 
   
   def get_current_dir():
      """Replace os.getcwd(), works even when launched by an IDE
      """
      # Thanks to 
      # https://stackoverflow.com/questions/2632199/
      #             how-do-i-get-the-path-of-the-current-executed-file-in-python
      #
      # Needs import inspect, os.path, os.sep
      filename = inspect.getframeinfo(inspect.currentframe()).filename
      path = os.path.dirname(os.path.abspath(filename))
      return path + os.sep

   filename = get_current_dir() + FILE
   
   print("[", tab[0], sep="", end=",\n")
   tab = imgfile2tab(filename)[:10][:10]
   for row in tab[:]:
      print(" ", row, ",", sep="")
   print("]")

